<?php
namespace Project\Controllers;

use Project\Admin\AbstractController;
use Project\Api\Lead\getLeadController;
use Project\Api\Lead\updateLeadController;
use Project\Api\Transactions\generatePdfController;
use Project\Api\User\getUserController;
use Project\Api\User\updateUserController;
use Project\Controllers\Administration\CompaniesController;
use Project\Controllers\Base\ValidateSellController;
use Project\Core\CoreController;
use Project\Core\CoreControllerManager;
use Project\Utilities\DirectoryManipulator;

class ControllerManager extends CoreControllerManager
{
    public function __construct()
    {
        /* Base */
        $this->addController('ExampleClass', 'complexe/path/to/test');
    }
}