<?php
namespace Project\Controllers\Utilities;

use Project\Security\ConnexionManager;
use Project\Core\CoreController;
use Project\Form\PasswordForget;
use Project\PageManager;

class PasswordForgetController extends CoreController
{
    public function __construct()
    {
        $this->setKey('password_forget');
    }

    protected function action()
    {
        $form = new PasswordForget();
        $form->process();
    }

}