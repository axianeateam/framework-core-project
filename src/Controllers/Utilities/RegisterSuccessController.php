<?php
namespace Project\Controllers\Utilities;

use Project\Security\ConnexionManager;
use Project\Core\CoreController;
use Project\PageManager;

class RegisterSuccessController extends CoreController
{
    public function __construct()
    {
        $this->setKey('register_success');
    }

    protected function action()
    {
        $mail = ConnexionManager::getFromSession('register_success');
        if(!$mail) { PageManager::goHome(); }

        $mail = ConnexionManager::getFromSession('register_success');
        PageManager::expose([
            'mail' => $mail
        ]);
        ConnexionManager::unsetFromSession('register_success');
    }

}