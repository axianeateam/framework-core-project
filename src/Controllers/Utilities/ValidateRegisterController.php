<?php
namespace Project\Controllers\Utilities;

use Project\Security\ConnexionManager;
use Project\Core\CoreController;
use Project\PageManager;

class ValidateRegisterController extends CoreController
{
    public function __construct()
    {
        $this->setKey('validate_register');
    }

    protected function action()
    {
        $mail = ConnexionManager::getRequest('mail');
        $token = ConnexionManager::getRequest('token');
        $user = PageManager::getMainDao()->getUserDao()->getUserByMail($mail);

        if(!$user || $user->getToken() !== $token || $user->getState() != 0) { PageManager::goHome(); return; }

        PageManager::getMainDao()->customUpdate(
            ['state = 1', 'token = ""'],
            'users',
            sprintf('mail = "%s"', $user->getMail())
        );

        PageManager::expose([
            'mail' => ConnexionManager::getRequest('mail'),
            'user' => PageManager::getMainDao()->getUserDao()->getUserByMail($mail)
        ]);
    }

}