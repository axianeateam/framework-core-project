<?php
namespace Project\Controllers\Utilities;

use Project\Security\ConnexionManager;
use Project\Core\CoreController;
use Project\Form\PasswordForget;
use Project\PageManager;

class PasswordModifierController extends CoreController
{
    public function __construct()
    {
        $this->setKey('password_forget');
    }

    protected function action()
    {
        $mail = ConnexionManager::getRequest('mail');
        $token = ConnexionManager::getRequest('token');
        $user = PageManager::getMainDao()->getUserDao()->getUserByMail($mail);

        if(ConnexionManager::isConnected()) {
            $user = ConnexionManager::getAccount();
        }

        if(!$user ||  (!ConnexionManager::isConnected() && $user->getPasswordToken() !== $token) ) { PageManager::goHome(); }

        $newPassword = ConnexionManager::getRequest('newPassword');
        if($newPassword) {

            $result = PageManager::getMainDao()->getUserDao()->updateUserPassword($mail, $token, $newPassword);

            if($result) {
                ConnexionManager::addFlashSessionMessage('success', 'Mot de passe modifié avec succès.', 1);
            } else {
                ConnexionManager::addFlashSessionMessage('warning', 'une erreur est survenue dans la mise à jour.', 1);
            }
            PageManager::goHome();
        }

        PageManager::expose([
            'mail' => ConnexionManager::getRequest('mail'),
            'token' => ConnexionManager::getRequest('token')
        ]);
    }

}