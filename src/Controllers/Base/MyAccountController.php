<?php
namespace Project\Controllers\Base;

use Project\Security\ConnexionManager;
use Project\Core\CoreController;
use Project\Form\AccountModifier;
use Project\PageManager;

class MyAccountController extends CoreController
{
    public function __construct()
    {
        $this->setKey('myaccount');
    }

    protected function action()
    {
        $user = ConnexionManager::getAccount();

        $form = new AccountModifier();
        $form->validateExisting(ConnexionManager::getAccount());

        PageManager::expose([
            'user' => $user
        ]);
    }

}