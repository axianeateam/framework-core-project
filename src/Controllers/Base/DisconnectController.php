<?php
namespace Project\Controllers\Base;

use Project\Security\ConnexionManager;
use Project\Core\CoreController;
use Project\Form\AbstractFormLead;
use Project\Form\AccountModifier;
use Project\Form\Login;
use Project\Form\Register;
use Project\PageManager;

class DisconnectController extends CoreController
{
    public function __construct()
    {
        $this->setKey('disconnect');
    }

    protected function action()
    {
        ConnexionManager::unsetFromSession('user');

        ConnexionManager::addFlashSessionMessage('success', 'Déconnexion effectuée avec succès.', 1);
        PageManager::goHome();
    }

}