<?php
namespace Project\Controllers\Base;

use Project\PageManager;
use Project\Security\ConnexionManager;
use Project\Core\CoreController;
use Project\Form\AbstractFormLead;
use Project\Form\AccountModifier;
use Project\Form\Login;
use Project\Form\Register;

class RegisterController extends CoreController
{
    public function __construct()
    {
        $this->setKey('register');
    }

    protected function action()
    {
        $AdvancedForm = new Register();
        if($AdvancedForm->checkValidity()) {
            if($AdvancedForm->validation()) {
                ConnexionManager::addFlashSessionMessage('success', "Votre demande d'inscription a été réalisée avec succès !", 1);
            }
        }

        PageManager::expose([
           'AdvancedForm' => $AdvancedForm
        ]);
    }

}