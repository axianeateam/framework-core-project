<?php
namespace Project\Controllers\Base;

use Project\Security\ConnexionManager;
use Project\Core\CoreController;
use Project\Form\AbstractFormLead;
use Project\Form\AccountModifier;
use Project\Form\Login;

class LoginController extends CoreController
{
    public function __construct()
    {
        $this->setKey('login');
    }

    protected function action()
    {
        $form = new Login();
        $form->process();
    }

}