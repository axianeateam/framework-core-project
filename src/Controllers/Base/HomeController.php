<?php
namespace Project\Controllers\Base;

use Project\Security\ConnexionManager;
use Project\Core\CoreController;
use Project\Form\AbstractFormLead;
use Project\Form\AccountModifier;
use Project\Form\Login;

class HomeController extends CoreController
{
    public function __construct()
    {
        $this->setKey('home');
    }

    protected function action()
    {
    }

}