<?php
namespace Project;
use Core\CorePageManager;
use Project\Dao\AbstractDao;
use Project\Dao\SqlDao;
use Project\Security\ConnexionManager;
use Project\Security\Router;

class PageManager extends CorePageManager
{

    /**
     * @return SqlDao
     */
    static function getMainDao(): AbstractDao
    {
        return static::$Dao;
    }

    static public function getRouter(): Router
    {
        if(!static::$Router) { static::$Router = new Router(); }
        return static::$Router;
    }

    static function initializeConnexion()
    {
        $sqlDao = new SqlDao();
        static::$Dao = $sqlDao;
    }

    static function getMainMenuItems() {
        $user = ConnexionManager::getAccount();
        $mainMenu = [];

        if(ConnexionManager::getSecurity()->checkRouteSecurity('disconnect'))
            $mainMenu[] = new NavigationItem('deconnexion', 'fa fa-sign-out-alt', 'Déconnexion');

        if(ConnexionManager::getSecurity()->checkRouteSecurity('connexion'))
            $mainMenu[] = new NavigationItem('connexion','fas fa-sign-in-alt', 'Connexion');

        if(ConnexionManager::getSecurity()->checkRouteSecurity('inscription'))
            $mainMenu[] = new NavigationItem('inscription','fa fa-user-plus', 'Inscription');

        return $mainMenu;
    }

    static public function getSiteName(): string
    {
        return 'New Project';
    }

    //Setup the homepage here (as a page argument => for the router)
    static public function getHomePage(): string
    {
        if(ConnexionManager::isConnected()) {
            return 'home';
        }
        return 'connexion';
    }

    /**
     * Useful if you're project is not at the root of the server host (ex in localhost)
     * @return string
     */
    static function getBaseRoot()
    {
        return 'core-project';
    }

    static public function getApplicationVersion(): string
    {
        return '0.0.1';
    }

    static public function getFooter(){
        require_once ('./parts/footer.php');
    }
    static public function getScripts() {
        require_once('./parts/scripts.php');
    }
    static public function getStyles() {
        require_once('./parts/styles.php');
    }
    static public function getHeader() {
        require_once('./parts/header.php');
    }
    static public function getNavigation() {
        require_once('./parts/navigation.php');
    }
}