<?php
namespace Project\Form;

use Project\Models\AbstractDbClass;
use Project\Security\ConnexionManager;

class Login extends AbstractForm {

    protected function bindParameters(): array {
        return [
            "mail" => $this->getData('mail'),
            "password" => $this->getData('password')
        ];
    }
    public function validation($attachedObject = false) {
        $login = $this->bindParameters();
        if(!$login['mail'] || !$login['password']) { return false; }

        ConnexionManager::connexion($login['mail'], $login['password']);
        return true;
    }

    public function validateExisting(AbstractDbClass $object)
    {
        // TODO: Implement validateExisting() method.
    }

    protected function checkValidity(array $datas)
    {
        if(!isset($datas) || !$datas || !count($datas)) { return false; }
        return true;
    }


    public function __construct()
    {
        $this->addSchema('mail');
        $this->addSchema('password');
    }
}