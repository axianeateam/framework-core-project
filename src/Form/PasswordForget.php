<?php
namespace Project\Form;

use Project\Models\AbstractDbClass;
use Project\Security\ConnexionManager;
use Project\Utilities\Mailer;
use Project\PageManager;

class PasswordForget extends AbstractForm {

    protected function bindParameters()  {

    }
    public function validation($attachedObject = false) {
        $mail = ConnexionManager::getRequest('mail');
        if(!$mail) { return false; }

        $token = ConnexionManager::generateToken();

        //Check if mail account exists
        $AccountSearch = PageManager::getMainDao()->getDao('User')->getUserByMail($mail);

        if(!$AccountSearch) {
            ConnexionManager::addFlashSessionMessage('warning', sprintf("Attention: le compte %s n'existe pas.", $mail));
            return false;
        }

        $updateUser = PageManager::getMainDao()->customUpdate(
            sprintf('password_token = "%s"', $token),
            'users',
            sprintf('mail = "%s"', $mail)
        );
        if($updateUser) {
            $mailer = new Mailer();

            $mailer
                ->setTo($mail)
                ->setSubject('Réinitialisation de mot de passe')
                ->setMessage(
                    sprintf('
                    <p>Nous avons bien reçu votre demande de réinitialisation de mot de passe.</p>
                    <p>Si toutefois cette demande ne venait pas de vous, ignorez ce message.</p>
                    <p>Pour changer votre mot de passe, veuillez cliquer sur <a href="http:%s?mail=%s&token=%s">ce lien</a></p>
                    <br><br>
                    <p><strong>Plus d\'informations sur l\'origine de la modification</strong></p>
                    <ul>
                        <li>Source : %s</li>
                        <li>Demande effectuée le : %s</li>
                    </ul>
                    <br>
                    <p>Merci.</p>
                    ',
                        PageManager::getPageUrl('password_modifier'),
                        $mail, $token, $_SERVER['HTTP_USER_AGENT'], date('d/m/Y à H:i:s', $_SERVER['REQUEST_TIME']))
                )->sendMail();
            ConnexionManager::addFlashSessionMessage('success', "Un mail de modification de mot de passe a bien été envoyé à l'adresse renseignée.", 1);
        }

        PageManager::goTo('login');
    }

    public function validateExisting(AbstractDbClass $object)
    {
    }

    protected function checkValidity(array $datas)
    {
        if(ConnexionManager::getRequest('mail')) { return true; }
        return false;
    }


    public function __construct()
    {
    }
}