<?php
namespace Project\Form;

use Delight\FileUpload\FileUpload;
use Project\Models\AbstractDbClass;
use Project\Security\ConnexionManager;
use Project\PageManager;
use Project\Models\User;

class AccountModifier extends AbstractForm {

    protected function bindParameters(): User {
        $User = new User();
        $User
            ->setName($this->getData('name'))
            ->setPassword($this->getData('password'))
            ->setFirstname($this->getData('firstname'))
            ->setMail($this->getData('mail'))
            ->setMailSecondary($this->getData('mail_secondary'))
            ->setCity($this->getData('city'))
            ->setZipcode($this->getData('zipcode'))
            ->setAddress($this->getData('address'))
            ->setPhoneMain($this->phoneField($this->getData('phone_main')))
            ->setPhonePro($this->phoneField($this->getData('phone_pro')));
        return $User;
    }
    protected function validation() {
        $User = $this->bindParameters();

        $User->save();
    }

    public function validateExisting(AbstractDbClass $object)
    {
        if(!$object->bindParametersToObject($_REQUEST)) { return false; }

        if(isset($_FILES['new_logo']) && $_FILES['new_logo']['size'] > 0) {
            $upload = new FileUpload();
            $upload->withTargetDirectory('assets/images/users');
            $upload->from('new_logo');
            $upload->withAllowedExtensions([ 'jpeg', 'jpg', 'png', 'gif' ]);
            $upload->withMaximumSizeInMegabytes(2);
            $upload->withTargetFilename(str_replace(' ', '_', strtolower($object->getName().$object->getFirstname() )));

            $file = $_FILES[$upload->getSourceInputName()];

            //Convert format to a good file extension
            $extension = str_replace('image/','.', $file['type']);
            $extension = str_replace('jpeg','jpg',$extension);

            try {
                $upload->save();
                $object->setIcon($upload->getTargetFilename().$extension);
            } catch (Error $e) {
            } catch (FileTooLargeException $e) {
            } catch (InputNotFoundException $e) {
            } catch (InvalidExtensionException $e) {
            } catch (InvalidFilenameException $e) {
            } catch (UploadCancelledException $e) {
            }
        }

        if($object->save()) {
            ConnexionManager::addFlashSessionMessage('success', 'Votre compte a bien été modifié', 1);
            PageManager::goTo(ConnexionManager::getRequest('page'));
        }
    }

    protected function checkValidity(array $datas)
    {
        if(!isset($datas) || !$datas || !count($datas)) { return false; }
        $User = new User();
        $User->bindParametersToObject($datas);
        if(!$User->isComplete()) { return false; }
        return true;
    }


    public function __construct()
    {
        $props =  User::getObjectProperties();
        foreach ($props as $prop) {
            $this->addSchema($prop->name);
        }
    }
}