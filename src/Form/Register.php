<?php
namespace Project\Form;

use Project\Models\AbstractDbClass;
use Project\Security\ConnexionManager;
use Project\Utilities\DateConverter;
use Project\Utilities\Mailer;
use Project\PageManager;
use Project\Models\User;

class Register extends AdvancedAbstractForm {

    public function __construct(){
        $this->setPrefix('form');
        $this->addSchema('name','string', 'text', ['label' => 'Nom *', 'required' => true]);
        $this->addSchema('firstname','string', 'text', ['label' => 'Prénom *', 'required' => true]);
        $this->addSchema('mail','string', 'mail', ['label' => 'Adresse Mail *', 'required' => true]);
        $this->addSchema('mail_confirm','string', 'mail', ['label' => 'Confirmation Adresse Mail *', 'required' => true]);
        $this->addSchema('mail_secondary','string', 'mail', ['label' => 'Adresse Mail de secours']);
        $this->addSchema('phone_main','string', 'tel', ['label' => 'Téléphone *', 'required' => true]);

        $this->addSchema('city','string', 'text', ['label' => 'Ville *', 'required' => true]);
        $this->addSchema('zipcode','string', 'text', ['label' => 'Code Postal *', 'required' => true]);
        $this->addSchema('address','string', 'text', ['label' => 'Adresse']);

        $this->addSchema('password','string', 'password', ['label' => 'Mot de passe *', 'required' => true]);
        $this->addSchema('password_confirm','string', 'password', ['label' => 'Confirmation du Mot de passe *', 'required' => true]);
    }

    protected function sendMailInscriptionConfirmation(User $User)
    {
        $tokenResult = PageManager::getMainDao()->customSelect(
            'token',
            'users',
            sprintf('mail = "%s"', $User->getMail())
        );

        $token = false;
        if($tokenResult && isset($tokenResult[0])) { $token = $tokenResult[0]['token']; }

        if(!$token) {
            ConnexionManager::addFlashSessionMessage('warning', "Une erreur s'est produite lors de l'envoi du mail de confirmation,
            merci de bien vouloir contacter l'équipe technique.", 1);
            return false;
        }

        $mail = new Mailer();
        $mail
            ->setTo($User->getMail())
            ->setSubject("Confirmation d'inscription")
            ->setMessage(
                sprintf("
                    <p>Bonjour, nous avons bien reçu votre demande d'inscription.</p>
                    <p>Pour la compléter, veuillez cliquer sur 
                    <a href='http:%s?mail=%s&token=%s'>ce lien</a></p>",
                    PageManager::getPageUrl('validation-inscription'),
                    $User->getMail(),
                    $token
                )
            );

        $mail->sendMail();
    }

    public function checkValidity(): bool
    {
        foreach ($this->getSchema() as $key =>  $element) {
            if(!isset($element['params'])) { continue; }
            if(!isset($element['params']['required'])) { continue; }
            if(!$element['params']['required']) { continue; }

            $value = $this->getSchemaValue($key, true);
            if($value === '') { return false; }
            if($value === null) { return false; }
            if($value === false) { return false; }
        }
        return true;
    }

    public function validation($attachedObject = false)
    {
        $User = new User();
        $User->bindParametersToObject($this->extractSchemaToParams());
        if(!$User->isComplete()) { return false; }

        $errors = [];

        /**
         * Do your stuff here to test datas
         */


        $searchAccount = PageManager::getMainDao()->getDao('User')->getUserByMail($User->getMail());
        if($searchAccount) { $errors[] = sprintf("L'adresse mail %s est déjà utilisée.", $User->getMail()); }

        $confirmPassword = $this->getSchemaValue('password_confirm');
        $confirmMail = $this->getSchemaValue('mail_confirm');

        if($confirmMail!== $User->getMail()) { $errors[] = 'Les adresses mails ne correspondent pas.'; }
        if($confirmPassword !== $User->getPassword()) { $errors[] = 'Les mots de passes ne correspondent pas.'; }

        if($errors) {
            foreach ($errors as $error) { ConnexionManager::addFlashSessionMessage('warning',$error); }
            return false;
        }


        /**
         * Do your stuff here before save
         */

        $User->setPassword(password_hash($User->getPassword(), PASSWORD_DEFAULT));
        $User->setToken(ConnexionManager::generateToken());
        $User->setCreated(DateConverter::newDatetime());

        if(!$User->save()){ return false; }

        ConnexionManager::setToSession('register_success', $User->getMail());

    }

    public function validateExisting(AbstractDbClass $object)
    {

    }
}