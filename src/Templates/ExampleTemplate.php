<?php
namespace Project\Templates;

class ExampleTemplate extends AbstractTemplate
{
    public static function renderExample()
    {
        ?>
        <p class="text-success">Merci d'avoir testé note exemple !</p>
        <?php
    }
}