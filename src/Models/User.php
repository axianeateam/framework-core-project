<?php
namespace Project\Models;

use Project\PageManager;

class User extends AbstractDbClass
{
    /* database and main datas */
    private $id;
    private $mail;
    private $mail_secondary;
    private $password;
    private $name;
    private $icon;
    private $firstname;
    private $type;
    private $city;
    private $zipcode;
    private $address;
    private $phone_main;
    private $state;
    private $created;
    private $last_login;
    private $token;
    private $password_token;

    public function __construct()
    {

        $this->addProtectedProperties('staff');
        $this->addAllowedProperties('update', [
            'mail_secondary','name','icon','firstname', 'city','zipcode','address','phone_main'
        ]);
        $this->addAllowedProperties('create', [
            'name', 'firstname', 'type', 'password', 'city', 'zipcode', 'address', 'phone_main', 'mail', 'mail_secondary', 'state', 'created', 'token'
        ]);
    }

    public function isComplete(): bool
    {
        if(!$this->getName()) { return false; }
        if(!$this->getFirstname()) { return false; }
        if(!$this->getMail()) { return false; }
        if(!$this->getPhoneMain()) { return false; }
        if(!$this->getPhonePro()) { return false; }

        return true;
    }

    /*
     * Getters and Setters
     *
     */


    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }


    public function setPassword($password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     * @return User
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMailSecondary()
    {
        return $this->mail_secondary;
    }

    /**
     * @param mixed $mail_secondary
     * @return User
     */
    public function setMailSecondary($mail_secondary)
    {
        $this->mail_secondary = $mail_secondary;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    public function getFullname()
    {
        return sprintf('%s %s', $this->getName(), $this->getFirstname());
    }

    /**
     * @param bool $onlyFile
     * @return mixed
     */
    public function getIcon($onlyFile = true)
    {
        if($onlyFile) { return $this->icon; }
        if(!$this->icon) { return 'axianea.png'; }
        return 'users/'.$this->icon;
    }

    /**
     * @param mixed $icon
     * @return User
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return User
     */
    public function setType($type)
    {
        $this->type = intval($type);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     * @return User
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoneMain()
    {
        return $this->phone_main;
    }

    /**
     * @param mixed $phone_main
     * @return User
     */
    public function setPhoneMain($phone_main)
    {
        $this->phone_main = $phone_main;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhonePro()
    {
        return $this->phone_pro;
    }

    /**
     * @param mixed $phone_pro
     * @return User
     */
    public function setPhonePro($phone_pro)
    {
        $this->phone_pro = $phone_pro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    public function setState($state): User
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     * @return User
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastLogin()
    {
        return $this->last_login;
    }

    /**
     * @param mixed $last_login
     * @return User
     */
    public function setLastLogin($last_login)
    {
        $this->last_login = $last_login;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     * @return User
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPasswordToken()
    {
        return $this->password_token;
    }

    /**
     * @param mixed $password_token
     * @return User
     */
    public function setPasswordToken($password_token)
    {
        $this->password_token = $password_token;
        return $this;
    }

}