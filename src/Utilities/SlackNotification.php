<?php
namespace Project;

class SlackNotification
{
    private $baseUrl;
    private $apiUrl;
    private $token;
    private $channel;
    private $icon_url;
    private $username;
    private $text;

    public function __construct()
    {
        $this->token = getenv('SLACK_TOKEN');
        $this->setBaseUrl('https://slack.com/api/');
    }

    public function send()
    {
        if(!$this->getApiUrl()) { return false; }
        if(!$this->getChannel()) { return false; }

        $params = [
            'token' => $this->token,
            'channel' => $this->getChannel(),
            'icon_url' => $this->getIconUrl(),
            "username" => $this->getUsername(),
            "text" =>  $this->getText()
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$this->getBaseUrl().$this->getApiUrl());
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close ($ch);
        return true;
    }

    /**
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param mixed $baseUrl
     * @return SlackNotification
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApiUrl()
    {
        return $this->apiUrl;
    }

    /**
     * @param mixed $apiUrl
     * @return SlackNotification
     */
    public function setApiUrl($apiUrl)
    {
        $this->apiUrl = $apiUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param mixed $channel
     * @return SlackNotification
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIconUrl()
    {
        return $this->icon_url;
    }

    /**
     * @param mixed $icon_url
     * @return SlackNotification
     */
    public function setIconUrl($icon_url)
    {
        $this->icon_url = $icon_url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return SlackNotification
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     * @return SlackNotification
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }
}