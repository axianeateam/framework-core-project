<?php
namespace Project\Security;

use Core\CoreRouter;

class Router extends CoreRouter {
    protected function initializeRoutes()
    {
        $user = ConnexionManager::getAccount();
        //Account Tool
        $this->addRoute('disconnect', 'deconnexion', 'IS_CONNECTED');
        $this->addRoute('password_forget', 'mot-de-passe-oublie', 0);
        $this->addRoute('password_modifier', 'modification-mot-de-passe', 'IS_CONNECTED');
        $this->addRoute('validate_register', 'validation-inscription', 0);
        $this->addRoute('register_success', 'inscription-succes', 0);

        //Basic
        $this->addRoute('register', 'inscription', 'IS_NOT_CONNECTED');
        $this->addRoute('login', 'connexion', 'IS_NOT_CONNECTED');
        //--

        //User panel
        $this->addRoute('', 'accueil', 'IS_CONNECTED');
        $this->addRoute('home', 'accueil', 'IS_CONNECTED');
        $this->addRoute('my_account', 'mon-espace', 'IS_CONNECTED');

    }
}