<?php
namespace Project\Security;

use Project\Security\Rules\IsConnected;

class Security extends AbstractSecurity
{
    /*
     * Now in the construct, you are able to store ONLY the rules with property modified.
     * Others are get automaticly.
     *
     */
    public function __construct()
    {
        $this->rules['IS_NOT_CONNECTED'] = new IsConnected();
        $this->rules['IS_NOT_CONNECTED']->setResult(false);
    }
}