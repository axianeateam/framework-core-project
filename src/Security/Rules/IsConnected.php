<?php
namespace Project\Security\Rules;


use Project\Security\ConnexionManager;
use Project\PageManager;
use Project\Models\User;

class IsConnected extends AbstractRule
{
    public function __construct()
    {
       $this->setId('IS_CONNECTED');
       $this->setName('Vérification du statut "connecté"');
    }

    public function action()
    {

    }

    public function process(): bool
    {
        $user = ConnexionManager::getAccount();

        if(!$user) { return false; }
        if(!$user instanceof User) { return false; }
        if(!$user->isComplete()) { return false; }
        if($user->getState() == -2) { return false; }

        return true;
    }

}