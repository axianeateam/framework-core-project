<?php
namespace Project\Security;

use Core\CoreConnexionManager;
use Project\Models\User;
use Project\PageManager;
use Project\Utilities\DateConverter;

class ConnexionManager extends CoreConnexionManager
{

    static public function checkAuth(): void
    {
        if(static::isConnected()) { static::updateAccountData(); }
    }

    protected static function updateAccountData()
    {
        $account = static::getAccount();
        $user = PageManager::getMainDao()->getUserDao()->getUserById($account->getId());
        static::setToSession('user', $user);
    }

    /**
     *
     * Ajouter texte personaliser selon ce qui a été renseigné (parrain, manager etc..)
     * @param $mail
     * @param $password
     * @return bool
     */
    static public function connexion($mail, $password)
    {
        $hash = PageManager::getMainDao()->getUserDao()->getAccountHash($mail);
        if(!password_verify($password, $hash)) {
            static::addFlashSessionMessage('primary', 'Le mail ou le mot de passe est incorrect');
            return false;
        }
        $user = PageManager::getMainDao()->getUserDao()->getUserByMail($mail);

        if(!$user || !$user->isComplete()) {
            static::addFlashSessionMessage('primary', 'Compte Inexistant.');
            return false;
        }

        if($user->getState() == -2) {
            static::addFlashSessionMessage('warning', 'Votre compte est suspendu pour le moment.');
            return false;
        }
        if($user->getState() == -1) {
            static::addFlashSessionMessage('warning', 'Votre demande a été rejeté en amont. Contactez '.PageManager::getSiteName()." pour plus d'informations.");
            return false;
        }
        if($user->getState() == 0) {
            static::addFlashSessionMessage('warning', 'Vous devez valider votre adresse mail avant de pouvoir vous connecter.');
            return false;
        }
        if($user->getState() == 1) {
            static::addFlashSessionMessage('warning', "Votre compte n'a pas encore été validé en amont.");
            return false;
        }


        static::setToSession('user', $user);

        //static::addFlashSessionMessage('primary','Bonjour '.$user->getFirstname().' ! Dernière connexion le '.date('d/m/Y à H\hi', strtotime($user->getLastLogin())));
        PageManager::getMainDao()->customUpdate(
            [sprintf('last_login = "%s"', DateConverter::newDatetime())],
            'users',
            sprintf('id = %d', $user->getId())
        );

        if(!$user->getLastLogin()) {
            PageManager::goTo('first-steps');
            return true;
        }

        PageManager::goHome();
        return true;
    }

    /**
     * @return User
     */
    static public function getAccount(): User
    {
        $account = static::getFromSession('user');
        if(!$account) { $account = new User(); }
        return $account;
    }
}