<?php
namespace Project\Dao;

use Project\Security\ConnexionManager;
use Project\Utilities\DateConverter;
use Project\Enum\LeadState;
use Project\PageManager;
use Project\Models\User;
use Project\Utilities\SimpleTimePerfTest;
use Project\Utilities\SqlPerfTest;

/**
 * Class UserDao
 * @package Project\Dao
 * @method $this->getUser(): User
 * @method $this->getUsers(): User
 */
class UserDao extends AbstractDao {

    public function __construct(\mysqli $mysqli)
    {
        $this->mysqli = $mysqli;

        $this->tableName = 'users';
        $this->modelName = 'User';
    }

    public function getAccountHash($mail)
    {
        $request = sprintf('SELECT password FROM users WHERE mail = "%s"', $mail);
        $result = $this->mysqli->query($request);
        if($this->isInError()) { return false; }

        $result = $result->fetch_assoc();
        if (!$result) { return false; }
        if (!isset($result['password'])) { return false; }
        return $result['password'];
    }

    public function updateUser(User $user)
    {
        $Performance = new SimpleTimePerfTest();

        $request = sprintf('
            UPDATE `users` 
            SET `name`= "%s",`firstname`= "%s",`icon`= "%s", `city`= "%s",`zipcode`= "%s",`address`= "%s",`phone_main`= "%s",
            `phone_pro`= "%s", `mail_secondary`= "%s"
            WHERE id = %d
        ',
            $this->mysqli->escape_string($user->getName()),
            $this->mysqli->escape_string($user->getFirstname()),
            $this->mysqli->escape_string($user->getIcon(true)),
            $this->mysqli->escape_string($user->getCity()),
            $this->mysqli->escape_string($user->getZipcode()),
            $this->mysqli->escape_string($user->getAddress()),
            $this->mysqli->escape_string($user->getPhoneMain()),
            $this->mysqli->escape_string($user->getPhonePro()),
            $this->mysqli->escape_string($user->getMailSecondary()),
            $user->getId()
        );

        $result = $this->mysqli->query($request);
        if($this->isInError("Erreur dans la mise à jour de l'utilisateur.")) { return false; }

        SqlPerfTest::addQuery('UserDao', 'users','updateUser', $Performance->getTestResult());


        return $result;
    }


    public function updateUserPassword($mail, $token, $newPassword)
    {
        $specialCond = sprintf('AND `password_token` = \"%s\"', $token);
        if(!$token) { $specialCond = ""; }

        $request = sprintf('
            UPDATE `users` 
            SET `password` = "%s", `password_token` = ""
            WHERE `mail` = "%s" %s
        ',
            password_hash($newPassword, PASSWORD_DEFAULT),
            $mail,
            $specialCond
        );

        $result = $this->mysqli->query($request);
        if($this->isInError("Une erreur est survenue dans le changement du mot de passe.")) { return false; }

        return $result;
    }

    public function createUser(User $User)
    {
        $manager = $User->getManager();
        $request = sprintf('
          INSERT INTO `users`(
              `id`, `name`, `firstname`, `type`, `password`, `city`, `zipcode`, `address`, `phone_main`, 
              `phone_pro`, `mail`, `mail_secondary`, `state`, 
              `created`, `token`
          ) 
          VALUES (
          NULL , "%s", "%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", %d, "%s", "%s"
          )',
            $this->mysqli->escape_string($User->getName()),
            $this->mysqli->escape_string($User->getFirstname()),
            0,
            password_hash($User->getPassword(), PASSWORD_DEFAULT),
            $this->mysqli->escape_string($User->getCity()),
            $this->mysqli->escape_string($User->getZipcode()),
            $this->mysqli->escape_string($User->getAddress()),
            $this->mysqli->escape_string($User->getPhoneMain()),

            $this->mysqli->escape_string($User->getPhonePro()),
            $this->mysqli->escape_string($User->getMail()),
            $this->mysqli->escape_string($User->getMailSecondary()),
            0,
            DateConverter::newDatetime(),
            ConnexionManager::generateToken()
        );


        $result = $this->mysqli->query($request);
        if($this->isInError("La création de l'utilisateur a echoué")) { return false; }

        return $this->mysqli->insert_id;
    }
}