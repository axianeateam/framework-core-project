<?php
namespace Project\Dao;

use Project\Utilities\DirectoryManipulator;
use ReflectionClass;

class SqlDao extends AbstractDao
{
    private $Daos = [];

    public function __construct()
    {
        $this->mysqli = new \mysqli(
            getenv('DATABASE_HOST'),
            getenv('DATABASE_USER'),
            getenv('DATABASE_PASSWORD'),
            getenv('DATABASE_NAME')
        );


        if ($this->mysqli->connect_errno) {
            echo printf("Échec de la connexion : %s\n", $this->mysqli->connect_error);
            exit();
        }

        $this->autoloadDaos($this->mysqli);
    }

    private function autoloadDaos(\mysqli $Mysqli)
    {
        foreach (DirectoryManipulator::getDirectory(__DIR__) as $DaoFile) {
            $DaoFile = str_replace('.php', '', $DaoFile);
            $completePath = sprintf('%s\\%s', __NAMESPACE__, $DaoFile);

            if(get_class($this) === $completePath) { continue; }
            if(!class_exists($completePath)) { continue; }
            if($completePath == AbstractDao::class) { continue; }

            $newDao = new $completePath($Mysqli);
            if(!$newDao instanceof AbstractDao) { continue; }

            $DaoFile = str_replace('Dao','', $DaoFile);
            $this->addDao($DaoFile, $newDao);
        }
    }

    /**
     * @return UserDao
     */
    public function getUserDao(): UserDao
    {
        return $this->getDao('User');
    }

    private function addDao($name, AbstractDao $Dao)
    {
        $this->Daos[$name] = $Dao;
    }

    public function getDao($name): AbstractDao
    {
        if(!isset($this->Daos[$name])) { return $this; }
        return $this->Daos[$name];
    }

    public function findDaoFromClass($class)
    {
        $className = (new ReflectionClass($class))->getShortName();
        return $this->getDao($className);
    }
}