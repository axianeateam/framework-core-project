<?php
namespace Project\Enum;

class UserType
{
    static public $map = [
        0 => ["name" => "Example"]
    ];

    static public $ref = [
    ];

    static public function isExample(int $value): bool {
        if($value == 0) { return true; }
        return false;
    }
}