SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: ``
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
    `id` int(11) NOT NULL,
    `mail` varchar(255) NOT NULL,
    `name` varchar(255) NOT NULL,
    `firstname` varchar(255) NOT NULL,
    `type` int(11) NOT NULL,
    `state` int(11) NOT NULL DEFAULT '0',
    `password` varchar(255) NOT NULL,
    `icon` varchar(90) DEFAULT NULL,
    `city` varchar(255) DEFAULT NULL,
    `zipcode` varchar(255) DEFAULT NULL,
    `address` varchar(255) DEFAULT NULL,
    `phone_main` varchar(255) DEFAULT NULL,
    `mail_secondary` varchar(255) NOT NULL,
    `created` datetime NOT NULL,
    `last_login` datetime DEFAULT NULL,
    `token` varchar(255) DEFAULT NULL,
    `password_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD UNIQUE KEY `id` (`id`),
    ADD UNIQUE KEY `mail` (`mail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
