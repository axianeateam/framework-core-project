#Framework

###Structure
The framework is divided in multiple part.
It's built on the MVC pattern.

- public : entry point and scope for users
    - assets : Where we store all the images/styles/scripts...
    - page : all of the project's render page
    - parts : base parts of the project's render structure
    - index.php : the index where **everything** is scoped
 - src : base of the `Project` namespace, all of the Project code

 - vendor : Extern libraries, autoload, read only.
    - Controllers : Place of every Project's Controllers
    - Core : All the Core classes
    - Dao : Place of every Daos/SubDaos
    - Enum : Every enumeration lists
    - Form : All Auto-Forms classes
    - Security : Place of Security with his Rules
    - Templates: TemplatesMethods used for Renders
    - Utilities : Utilities Class like Date/DataConverter, TestPerf...
    
    
  
####Router
The Router can be find in `Core\CoreRouter`.
That's an abstract class that is instantiate by default in the
`Core\CorePageManager`.

The Router class only has a `__construct` where you can find inside it all the routes
with the following syntax `$this->addRoute($args)`

Every Routes has a securityLevel (can be multiple) (described bellow).

####Security
The Project's Security is inside the `Project\Security` class that is instance by default
in the `Core\CoreConnexionManager` class.

The Security takes all the Rules (class in `Project\Security\Rules` namespace) automatically
and extends `Project\Security\Rules\AbstractRule` and store them.

You can define manually a rule in the `__construct` (like _Routes_)

A rule has a process that return a boolean.

A rule can have multiple parent and will return the **AND OPERATION** of all that must be true.

####Controllers
Controllers are automatically called by their name.
the `/test-page` route will automatically try to call `testPageController` or `testPage` (with the `Project\Controllers\Controllers` base namespace)

Like Rules, you can attach manually a controller with the same operation.

**Note:**

Controllers are the good-practice place to `PageManager::expose(['renderVarName' => $var])` your vars to your render.



####Renders
As said above, your renderer pages are called by the Router, you'll have the access of the exposed var and are free to do everything.



####Models
Models class extends `Project\Models\AbstractDbClass` that give you lot of things (not mentionned here).

In your PageManager you can find the `SqlDao` instantiate object. It have all
of the subDaos. SubDaos are used to group ready-to-use request.
@Todo : Daos will have their own-integrated rule system to prevent unwanted request !

**Note:**

A well defined structure, 
- Your Model attached to a subDao
- The SubDao has a getter method for the Model (like `getUser()`)

will allow you to use the powerful Object Autoload (defined in `Project\Models\AbstractDbClass`).

As much as you use it, the best perf you'll get. It reduce quite a lot the queries amount
and prevent lot of errors like `$user->getCompany()->getName()` where `$user->getCompany() = 3`

There's no reason to don't use it.


### Command List
composer update
composer full-update
#### Push sur le FTP
Attention: Pour push sur le FTP, vous devez avoir votre repo local clean (git status empty)

git-ftp push

#### Créer une nouvelle migration
vendor/bin/phinx create YourMigrationName  -c configs/phinx.php
#### Effectuer les migrations
composer run migration
vendor/bin/phinx migrate -e development -c configs/phinx.yml

#### Mettre à jour en prod
#####Attention:
Selon les configs serveurs, PHP ne se trouvera pas au même endroit.
Voici un exemple :

- /usr/bin/php7.1-cli "votre commande php"

Pour mettre à jour en prod, se connecter en SSH et effectuer un composer update
selon config serveur: 
- /usr/bin/php7.1-cli composer.phar update
