<?php

use Project\Security\ConnexionManager;
use Project\PageManager;
use Project\NavigationItem;
use Project\Templates\AbstractTemplate;

$user = ConnexionManager::getAccount();
$currentRoute = PageManager::getRouter()->getCurrentRoute();

$params = $currentRoute->extractParameters();
$pageSlug = $currentRoute->getSlug($params);

$mainMenu = PageManager::getMainMenuItems();


/**
 *
 @Todo: Debug safari quand fenêtre trop petite.
 *
 **/

?>
<nav class="main-nav navbar flex-column text-dark py-0 justify-content-start " id="main-navigation" style="min-height: 100%;">
    <a href="<?= PageManager::getPageUrl(''); ?>" class="company-logo-link">
        <img src="<?= PageManager::getAsset('images', 'your-logo-there'); ?>" alt="Logo" class="company-logo">
    </a>
    <ul class="navbar-nav flex-column nav-tabs position-sticky sticky-top">
        <?php
        /** @var NavigationItem $item**/
        foreach ($mainMenu as $item):
            $absoluteLink = PageManager::getPageUrl($item->getPage(), $item->getAdditional(), $item->getArg());
            $relativeLink = PageManager::getPageUrl($item->getPage(), $item->getAdditional(), $item->getArg(), false);
        ?>
        <li class="nav-item <?= AbstractTemplate::renderCondition($pageSlug == $relativeLink, 'active');?>">
            <a class="color-axianea nav-link"
               href="<?= $absoluteLink ?>">
                <?php if($relativeLink === 'mon-espace'): ?>
                    <img class="user-icon" src="<?= PageManager::getAsset('images', $user->getIcon(false)); ?>">
                <?php else: ?>
                    <i class="<?= $item->getIcon(); ?>"></i>
                <?php endif; ?>
                <span class="d-none d-lg-inline">
                    <?= $item->getTitle(); ?>
                </span>
            </a>
        </li>
        <?php endforeach; ?>

        <hr class="w-100 mx-auto">
        <div>
            <p class="text-muted text-right d-none d-lg-block m-0 copyright small">&copy; <?= PageManager::getSiteName();?> 2018 - <?= date('Y'); ?></p>
            <p class="text-muted text-right small m-0">v<?= PageManager::getApplicationVersion(); ?></p>
        </div>
    </ul>

    <div class="d-none d-lg-block" style="position: absolute; right: 0;">
        <button class="ml-auto d-block btn" id="reduceMenuBtn">
            <i class="fas fa-long-arrow-alt-left"></i>
        </button>
    </div>
</nav>