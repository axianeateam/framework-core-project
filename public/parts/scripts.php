<!-- Load Libraries -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>

<!-- SnackbarJS plugin -->
<script src="https://cdn.rawgit.com/FezVrasta/snackbarjs/1.1.0/dist/snackbar.min.js"></script>

<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>

<!--
<link rel="stylesheet" href="<?= \Project\PageManager::getAsset('scripts','bootstrap-material-design.min.js')?>">
-->

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script>
    $(document).ready(function() { $('body').bootstrapMaterialDesign(); });
    Chart.Legend.prototype.afterFit = function() {
        this.height = this.height + 15;
    };


    function getCompleteAppOrigin() {
        return '<?= \Project\PageManager::getProjectAbsoluteRoot(); ?>';
    }
</script>

<!-- Load Project Script -->
<script type="application/javascript" src="./assets/scripts/class/DataConverter.js"></script>
<script type="application/javascript" src="./assets/scripts/class/DomBinder.js"></script>
<script type="application/javascript" src="./assets/scripts/class/Lead.js"></script>
<script type="application/javascript" src="./assets/scripts/class/DomManipulator.js"></script>