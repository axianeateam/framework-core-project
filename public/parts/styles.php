<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<!--
<link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
-->
<link rel="stylesheet" href="<?= \Project\PageManager::getAsset('styles','bootstrap-material-design.min.css');?>">

<link rel="stylesheet" href="./assets/styles/main.css">

<link rel="stylesheet" href="./assets/styles/axianea.css">

<!--
<link rel="stylesheet" href="./assets/styles/material-dashboard.min.css">
-->