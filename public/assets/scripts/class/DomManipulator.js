var DomManipulator = {
    getSelectCurrentOption : function(selectElement) {
        var option = selectElement.querySelector('option[value="'+selectElement.value+'"]');
        if(!option) { return selectElement; }
        return option;
    }
};