var DataConverter = {
    price: function (price) {
        return parseInt(price).toLocaleString();
    },
    phone: function (phone) {
        var result = '';
        var phoneArray = phone.split('');
        for(var i = 0; i < phoneArray.length; i++) {
            result += phoneArray[i];
            if((i+1)%2 !== 0 || i+1 === phoneArray.length) { continue; }
            result += '.';
        }
        return result;
    }
};