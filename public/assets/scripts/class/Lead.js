var Lead = function () {
    this.id = null;
    this.name = null;
    this.firstname = null;
    this.mail = null;
    this.phone = null;
    this.city = null;
    this.zipcode = null;
    this.address = null;
    this.state = null;
    this.created = null;
    this.modified = null;
    this.interest = function() {
        this.id = null;
        this.type = null;
        this.typo = null;
        this.objectives = null;
        this.surface_min = 0;
        this.surface_max = 0;
        this.budget_min = 0;
        this.budget_max = 0;
        this.localization = null;
    };

    this.initialize = function() {
        this.interest = new this.interest();
    };
    this.initialize();
};