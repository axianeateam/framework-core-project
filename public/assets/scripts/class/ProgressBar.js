var ProgressBar = function() {
    this.progressBar = false;

    this.init = function (parent, position) {
        if(!position) { position = 'top'; }

        window.addEventListener('scroll', this.progress);
        progressBar = document.createElement('div');
        progressBar.classList.add('progressBar');
        progressBar.style.position = 'absolute';
        progressBar.style[position] = 0;
        progressBar.style.left = 0;
        progressBar.style.right = "100%";
        progressBar.style.height = "2px";
        progressBar.style.backgroundColor = "blue";
        progressBar.style.borderRadius = "5px";
        progressBar.style.transition = "0.1s all";
        progressBar.style.zIndex = "9000";

        if(parent && document.getElementById(parent)) {
            document.getElementById(parent).insertBefore(progressBar,document.getElementById(parent).firstChild);
            return;
        }

        progressBar.style.position = 'fixed';
        document.body.insertBefore(progressBar, document.body.firstChild);
    };

    this.progress = function () {

        var supportPageOffset = window.pageXOffset !== undefined;
        var isCSS1Compat = ((document.compatMode || "") === "CSS1Compat");

        var scrollHeight = document.body.scrollHeight;
        if(document.documentElement.scrollHeight != 0 ) {
            scrollHeight = document.documentElement.scrollHeight;
        }

        var clientHeight = document.body.clientHeight;
        if(document.documentElement.clientHeight != 0 ) {
            clientHeight = document.documentElement.clientHeight;
        }

        var maxHeight =  scrollHeight - clientHeight;

        var actualHeight = Math.round(supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop);

        var documentWidth = document.body.clientWidth;

        var scrollRatio =  Math.round(actualHeight / maxHeight *100);

        if( scrollRatio < 50) {
            progressBar.style.backgroundColor = "rgb(228,185,57)";
            progressBar.style.boxShadow = "1px 1px 1px 0px rgba(228,185,57,0.5)";
        } else if (scrollRatio < 90 ) {
            progressBar.style.backgroundColor = "rgb(3,169,244)";
            progressBar.style.boxShadow = "1px 1px 1px 0px rgba(3,169,244, 0.5)";
        } else {
            progressBar.style.backgroundColor = "rgb(205,220,57)";
            progressBar.style.boxShadow = "1px 1px 1px 0px rgba(205,220,57, 0.5)";
        }

        progressBar.style.right = 100 - scrollRatio+"%";
    }
};