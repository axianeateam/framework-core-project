/**
 * DomBinder : permet de suivre l'évolution d'une variable, séquence les modifications (changement, lors d'event...)
 * DomBinder.addBinding : Lors d'une modification de la variable attachée à DomBinder, execute la modif sur l'élément
 * DOM donné en param.
 *
 * @ElementBinder : params @element (Object) et @prop (property de l'objet), le tout donne l'élément à écouter.
 * @DomBinder s'instancie avec un @ElementBinder. Lorsque l'on créé un @DomBinder, on lui donne le path à écouter
 * (objet, variable...) puis nous pouvons ajouter des Bindings via @DomBinder.addBinding(element, attribute, event, specialCb)
 *
 *  @DomBinder.addBinding attend :
 * - un @element HTML sur lequel l'écouteur agira lorsqu'il y aura une modification.
 * - un @event (type eventlistener) pour mettre à jour sur spécial event (optionnel)
 * - un @specialCb dans lequel vous pouvez passer une function custom où ce qu'elle return sera bindé à l'element (optionnel)
 *
 * Exemple :

 var credAmount = document.getElementById('someHTML');
 var credits = {
    amount: 15,
    divider: 2
};

 new DomBinder(new ElementBinder(credits, 'amount')).addBinding(credAmount, 'innerHTML',null, function() {
    return credits.amount / credits.divider;
});

 * Cet exemple permet à l'élément HTML @credAmount d'être automatiquement mis à jour lorsque @credits.amount sera modifié
 * De plus, L'élément HTML affichera le résultat du @specialCb soit "credits.amount / credits.divider".
 **/

var ElementBinder = function(element, prop) {
    this.object = element;
    this.property = prop;
};

var DomSubscriber = function (element, attribute, event, specialCb) {
    this.element = element;
    this.attribute = attribute;
    this.event = event;
    this.specialCb = specialCb;
};

var DomBinderCollection = [];

var DomBinder = function (elementBinder) {
    this.elementBindings = [];
    _this = this;
    this.value = elementBinder.object[elementBinder.property];

    this.valueGetter = function() {
        return this.value;
    };

    this.valueSetter = function(value) {
        this.value = value;
        for(var i = 0; i < _this.elementBindings.length; i++) {
            console.log(_this);
            _this.doBinding(_this.elementBindings[i], value);
        }
    };

    this.doBinding = function(bindingElement, value) {
        //Default operation
        if(!bindingElement.specialCb) {
            bindingElement.element[bindingElement.attribute] = value;
            return;
        }
        bindingElement.element[bindingElement.attribute] = bindingElement.specialCb();
    };

    this.addBinding = function(element, attribute, event, specialCb){
        var domSub = new DomSubscriber(element, attribute, event, specialCb);
        if(event) {
            domSub.element.addEventListener(domSub.event, function() {
                _this.valueSetter(domSub.element[domSub.attribute]);
            });
        }

        this.elementBindings.push(domSub);
        this.doBinding(domSub, this.value);
        return this;

    };

    Object.defineProperty(elementBinder.object, elementBinder.property, {
        get: this.valueGetter,
        set: this.valueSetter
    });

    //elementBinder.object[elementBinder.property] = this.value;
    DomBinderCollection.push(this);
};