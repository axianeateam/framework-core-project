
function getArrayByProperty(datas, key)
{
    var results = [];
    for(var i in datas) {
        var data = datas[i];
        if(!data.hasOwnProperty(key)) { results.push(0); }
        results.push(data[key]);
    }
    return results;
}

function bindDataByLabels(datas, value, key, labels) {
    var results = [];
    for(var i = 0; i < labels.length; i ++) {
        var added = false;
        var label = labels[i];
        for(var j in datas) {
            var data = datas[j];
            if(data[key] == label) {
                results.push(data[value]);
                added = true;
            }
        }

        if(!added) { results.push(0); }
    }
    return results;
}

function getKeyN(element, n)
{
    if(!n) { n = 0; }
    return Object.keys(element)[n];
}

function getPropertyN(element, n) {
    return element[getKeyN(element, n)];
}

function generateChart(canvasName, arrayDatas) {
    var ctx = document.getElementById(canvasName).getContext('2d');

    var datasSets = [];
    var labels = [];
    for(var i = 0; i < arrayDatas.length; i++) {
        var object = arrayDatas[i];
        labels = Array.from(new Set(labels.concat(getArrayByProperty(object.datas, object.confs.labelKey))));
    }
    for(var i = 0; i < arrayDatas.length; i++) {
        var object = arrayDatas[i];

        var set = {
            label: object.confs.labels,
            data: bindDataByLabels(object.datas, object.confs.valueKey, object.confs.labelKey, labels),
            fill: object.confs.fill,
            type: object.confs.type,
            backgroundColor: object.confs.color,
            borderColor: object.confs.color,
            scale: 1,
            ticks: 0
        };

        if(object.confs.hasOwnProperty('dynamicLabel') && getPropertyN(object.datas, 0).hasOwnProperty(object.confs.dynamicLabel)) {
            set.label = getPropertyN(object.datas, 0)[object.confs.dynamicLabel];
        }

        datasSets.push(set);
    }

    if(!datasSets[0]) { return false; }

    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: datasSets[0].type,

        // The data for our dataset
        data: {
            labels: labels,
            datasets: datasSets
        },

        // Configuration options go here
        options: {
            responsive: true,
            layout: {
                padding: {
                }
            },
            legend: {
                labels: {
                    boxWidth: 15,
                    fontSize: 10
                }
            },
            scales: {
                xAxes: [{
                    stacked: false
                }],
                yAxes: [{
                    stacked: false,
                    ticks: {
                        min: 0,
                    }
                }]
            },
            title: {
                display: true
            },
            tooltips: {
                position: 'average',
                mode: 'index',
                intersect: false
            }
        }
    });
}