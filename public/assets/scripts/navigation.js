var reduceMenuBtn = document.getElementById('reduceMenuBtn');
if(reduceMenuBtn) {
    reduceMenuBtn.addEventListener('click', reduceMenu);
}

function reduceMenu() {
    var mainNavigation = document.getElementById('main-navigation');
    var texts = mainNavigation.querySelectorAll('.nav-item .nav-link span');
    for(var i = 0; i < texts.length; i++) {
        var text = texts[i];
        text.classList.toggle('hidden');
    }

    reduceMenuBtn.classList.toggle('rotated');
    mainNavigation.parentElement.classList.toggle('col-2');
    mainNavigation.parentElement.classList.toggle('col-1');
    mainNavigation.classList.toggle('reduced');
    document.getElementById('main-content').classList.toggle('col-10');
    document.getElementById('main-content').classList.toggle('col-11');
}