
function updateAssignedTo(value, key, id) {
    if(!value) { value = this.value; }
    if(!key) { key = this.getAttribute('name'); }

    if(!id) {
        var parent = this.parentElement.parentElement;
        id = parent.getAttribute('table-id');
    }

    var httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = function () { };
    httpRequest.open('GET', getCompleteAppOrigin()+'/api/lead/update/'+id+'?target=assigned&user_id='+value+'&' + '&values[]=' + value + '&keys[]=' + key, true);
    httpRequest.send();
}

/*********/
var DetailModal = function(modal){
    this.assigned = modal.querySelector('.assignedName');
    this.state = modal.querySelector('.state');
    this.creator = modal.querySelector('.creator');
    this.name = modal.querySelector('.name');
    this.address = modal.querySelector('.address');
    this.mail = modal.querySelector('.mail');
    this.phone = modal.querySelector('.phone');
    this.infos = modal.querySelector('.infos');

    this.created = modal.querySelector('.created');
    this.modified = modal.querySelector('.modified');

    this.objective = modal.querySelector('.objective');
    this.surface = modal.querySelector('.surface');
    this.budget = modal.querySelector('.budget');
    this.localization = modal.querySelector('.localization');
};
var detailModal = new DetailModal(document.getElementById('leadDetailModal'));

function leadDetailModal() {

    var id = this.getAttribute('table-id');

    var httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState !== 4) { return; }

        var lead = JSON.parse(this.responseText);
        var interest = lead.interest;

        detailModal.name.innerHTML = lead.name+' '+lead.firstname;

        detailModal.assigned.innerHTML = '<span class="badge badge-warning">Pas encore assigné</span>';
        if(lead.assigned && lead.assigned.hasOwnProperty('name')) {
            detailModal.assigned.innerHTML = '<span class="badge badge-axianea">Assigné à ' + lead.assigned.name + ' ' + lead.assigned.firstname+"</span>";
        }

        detailModal.creator.innerHTML = "Auteur: "+lead.creator.name+' '+lead.creator.firstname+' de '+lead.creator.company.name;

        detailModal.address.innerHTML = lead.address+', '+lead.zipcode+', '+lead.city;
        detailModal.phone.innerHTML = DataConverter.phone(lead.phone);
        detailModal.mail.innerHTML = lead.mail;
        detailModal.infos.innerHTML = lead.infos;

        var FormatedDate = new Date(lead.created);
        var createdParsed = FormatedDate.getUTCDate()+'/'+FormatedDate.getUTCMonth()+'/'+FormatedDate.getUTCFullYear()+' à '+FormatedDate.getUTCHours()+'h';

        var FormatedDate = new Date(lead.modified);
        var modified = FormatedDate.getUTCDate()+'/'+FormatedDate.getUTCMonth()+'/'+FormatedDate.getUTCFullYear()+' à '+FormatedDate.getUTCHours()+'h';

        detailModal.created.innerHTML = "Créé le "+createdParsed;
        detailModal.modified.innerHTML = "Modifié le "+modified;
        detailModal.objective.innerHTML = interest.type+' '+interest.typo+' ('+interest.objectives+')';
        detailModal.surface.innerHTML = "Entre "+interest.surface_min+" et "+interest.surface_max+" m2";
        detailModal.budget.innerHTML = "Entre "+DataConverter.price(interest.budget_min)+" et "+DataConverter.price(interest.budget_max)+" euros";

        var state = {
            "-2": {"name": "Sans Suite", "badge": "danger"},
            "-1": {"name": "Doublon", "badge": "secondary"},
            "0": {"name": "En Attente", "badge": "info"},
            "1": {"name": "Validé", "badge": "info"},
            "2": {"name": "Attente Traitement", "badge": "info"},
            "3": {"name": "Traitement en cours", "badge": "info"},
            "4": {"name": "Réservation", "badge": "primary"},
            "5": {"name": "Annulation", "badge": "danger"},
            "7": {"name": "Vente", "badge": "success"}
        };
        detailModal.state.innerHTML = '<span class="badge badge-'+state[lead.state].badge+'">'+state[lead.state].name+'</span>';

        detailModal.localization.innerHTML = '';

        for(var i = 0; i < interest.localization.length; i++) {
            detailModal.localization.innerHTML += '<span class="badge badge-axianea m-1">'+interest.localization[i]+'</span>';
        }

        $('#leadDetailModal').modal({
            keyboard: false
        });
    };
    httpRequest.open('GET', getCompleteAppOrigin()+'/api/lead/get/'+id, true);
    httpRequest.send();
}

/*********/


function rollbackState()
{
    var element = document.getElementById('updateStateModalTarget');
    element.removeAttribute('id');
    element.value = element.getAttribute('old-value');
}

function updateState()
{
    var leadId = this.getAttribute('table-id');
    var state = this.getAttribute('state-value');

    if(state == 7) {
        window.location = getCompleteAppOrigin()+'/prospects/'+leadId+'/creation-vente';
        return;
    }

    var httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = function () { };
    httpRequest.open('GET', getCompleteAppOrigin()+'/api/lead/update/'+leadId+'?target=state&values[]=' + state + '&keys[]=state', true);
    httpRequest.send();
    window.location.reload();

}

if(document.getElementById('updateStateModal')) {
    document.getElementById('updateStateModal').querySelector('button.validateBtn').addEventListener('click', updateState);
    document.getElementById('updateStateModal').querySelector('button.closeBtn').addEventListener('click', rollbackState);
}

function updateStateModal(element, oldValue) {
    var modal = document.getElementById('updateStateModal');

    var option = DomManipulator.getSelectCurrentOption(element);

    modal.querySelector('.modal-title').innerHTML = 'Confirmation';
    modal.querySelector('.modal-body').innerHTML =
        "<p>L'état du client va passer en: "+option.innerHTML+".</p><p>Attention de bien modifier le bon client.</p><p>Certains statuts archivent automatiquement le client.</p>";

    var leadId = element.parentElement.parentElement.parentElement.getAttribute('table-id');
    var btn = modal.querySelector('button.validateBtn');
    var cancelBtn =  modal.querySelector('button.closeBtn');

    btn.setAttribute('table-id', leadId);
    btn.setAttribute('state-value', element.value);
    btn.setAttribute('old-value', oldValue);

    element.setAttribute('id','updateStateModalTarget');

    $('#updateStateModal').modal({
        keyboard: false
    });
}

function AssignedToModal() {
    document.getElementById('assignedToModal').setAttribute(
        'element-id', this.getAttribute('table-id')
    );
    document.getElementById('assignedToModal').querySelector('.modal-title').innerHTML = 'Assigner la fiche client';

    $('#assignedToModal').modal({
        keyboard: false
    });
}



/***********/

$(function() {
    var inputUpdater = document.querySelectorAll('.state.inputUpdater');
    for (var i = 0; i < inputUpdater.length; i++) {
        var input = inputUpdater[i];
        var oldVal = input.value;
        input.setAttribute('old-value', oldVal);
        input.addEventListener('change', function(){
            updateStateModal(this, oldVal);
        });
    }

    var assignedToCollection = document.querySelectorAll('button.assignedToBtn');
    for (var i = 0; i < assignedToCollection.length; i++) {
        var input = assignedToCollection[i];

        input.addEventListener('click', AssignedToModal);
    }

    var detailBtnCollection = document.querySelectorAll('button.detailBtn');
    for (var i = 0; i < detailBtnCollection.length; i++) {
        var input = detailBtnCollection[i];

        input.addEventListener('click', leadDetailModal);
    }

    var assignedToModal = document.getElementById('assignedToModal');
    if(assignedToModal) {
        var submit = assignedToModal.querySelector('.validateBtn');
        submit.addEventListener('click', function () {
            updateAssignedTo(assignedToModal.querySelector('.assignedToSelect').value, 'assigned', assignedToModal.getAttribute('element-id'));
            window.location = '';
        });
    }

});