<section class="container card my-4 card-body">
    <p class="h6">Merci <?= $user->getName(). ' '. $user->getFirstname(); ?>, votre compte a bien été validé.</p>
    <p class="my-1">Vous pourez vous connecter à l'aide de votre mail principal (<?= $user->getMail(); ?>) et du mot de passe renseigné
    une fois votre compte validé.</p>
    <a href="<?= \Project\PageManager::router('login'); ?>">Retour à l'espace de connexion</a>
</section>