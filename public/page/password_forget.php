<?php
use Project\Security\ConnexionManager;

?>
<section class="container-fluid row justify-content-center">
    <section class="card my-4 col-4">
        <form class="card-body" method="POST">
            <?php if(!ConnexionManager::isConnected()): ?>
            <h1>Mot de passe oublié</h1>
            <?php else: ?>
            <h2>Modifier mon mot de passe</h2>
            <?php endif; ?>
            <section>
                <?php if(!ConnexionManager::isConnected()): ?>
                <p class="m-0">Ne vous inquiétez pas, ça arrive.</p>
                <p>Renseignez l'adresse mail de votre compte et on s'occupe de tout.</p>
                <div class="form-group">
                    <label class="bmd-label-floating" for="mail">Mail</label>
                    <input type="email" name="mail" id="mail" class="form-control" required="required" />
                </div>
                <?php else: ?>
                    <input type="email" name="mail" id="mail" class="form-control" value="<?= ConnexionManager::getFromSession('user')->getMail();?>" hidden required="required" />
                <?php endif; ?>
                <section class="row mt-4">
                    <button class="btn btn-axianea ml-auto">Changer de mot de passe</button>
                </section>
                <?php if(!ConnexionManager::isConnected()): ?>
                    <a class="btn d-block ml-auto" href="<?= \Project\PageManager::router('register'); ?>">Nouveau ? Inscription !</a>
                    <a class="btn d-block ml-auto" href="<?= \Project\PageManager::router('login'); ?>">Je m'en souviens au final</a>
                <?php else: ?>
                    <ul class="list-group py-4">
                        <li class="list-group-item active">Comment ça marche ?</li>
                        <li class="list-group-item">On vous envoies un mail</li>
                        <li class="list-group-item">Dans ce mail se trouvera un lien de confirmation</li>
                        <li class="list-group-item">Vous changez votre mot de passe</li>
                        <li class="list-group-item">Terminé !</li>
                    </ul>
                    <a class="btn d-block ml-auto" href="<?= \Project\PageManager::router('login'); ?>">Au final pas besoin !</a>
                <?php endif; ?>
            </section>
        </form>
    </section>
</section>