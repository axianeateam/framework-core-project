<section class="my-4">
    <section class="card card-body">
        <h4 class="text-success">Félicitation !</h4>
        <p>Votre demande d'inscription a bien été enregistrée. Vous allez recevoir un mail de confirmation à l'addresse <strong><?= $mail;?></strong>.</p>
        <p>Une fois validé, votre manager recevra une notification d'inscription qu'il pourra valider à son tour afin d'activer votre compte.</p>
        <p>Merci et à très bientôt !</p>
        <a href="<?= \Project\PageManager::router('login'); ?>">Retour à l'accueil</a>
    </section>
</section>
