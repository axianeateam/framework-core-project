
<section class="row justify-content-center">
    <section class="mb-4 col-12 col-md-auto">
        <h1 class="card card-body my-4 h3">Inscription</h1>
        <form class="" method="POST">
            <section class="mb-4 card card-body">
                <h4>Informations de compte</h4>
                <section class="row">
                    <div class="col-md col-sm-12">
                        <div class="form-group">
                            <label class="bmd-label-floating" for="name">Nom</label>
                            <input type="text" name="name" id="name" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label class="bmd-label-floating" for="firstname">Prénom</label>
                            <input type="text" name="firstname" id="firstname" class="form-control" required="required">
                        </div>
                    </div>

                    <div class="col-md col-sm-12">
                        <div class="form-group">
                            <label class="bmd-label-floating" for="mail">Adresse Mail</label>
                            <input type="email" name="mail" id="mail" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label class="bmd-label-floating" for="mail_secondary">Adresse Mail secondaire</label>
                            <input type="email" name="mail_secondary" id="mail_secondary" class="form-control" required="required">
                        </div>
                    </div>
                </section>

                <div class="form-group">
                    <label class="bmd-label-floating" for="password">Mot de Passe</label>
                    <input type="password" name="password" id="password" class="form-control" required="required">
                </div>

                <section class="row">
                    <div class="col-md-3 col-xs-12 form-group">
                        <label class="bmd-label-floating" for="city">Ville</label>
                        <input type="text" name="city" id="city" class="form-control" required="required">
                    </div>
                    <div class="col-md-3 col-xs-12 form-group">
                        <label class="bmd-label-floating" for="zipcode">Code Postal</label>
                        <input type="text" name="zipcode" id="zipcode" class="form-control" required="required">
                    </div>
                    <div class="col-md-6 col-xs-12 form-group">
                        <label class="bmd-label-floating" for="address">Adresse</label>
                        <input type="text" name="address" id="address" class="form-control" required="required">
                    </div>
                </section>

                <section class="row">
                    <div class="col-md col-xs-12 form-group">
                        <label class="bmd-label-floating" for="phone_main">Téléphone</label>
                        <input type="tel" name="phone_main" id="phone_main" class="form-control" required="required">
                    </div>
                </section>

                <button class="btn btn-axianea my-2 ml-auto mr-4 d-block">Valider</button>
            </section>
        </form>
    </section>
</section>