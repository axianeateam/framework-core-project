<?php
use Project\Security\ConnexionManager;
use Project\Utilities\DataConverter;
use Project\Enum\UserType;
?>
<section class="container my-4">
    <h1 class="card card-body h4 mb-4">Bonjour <?= $user->getName(); ?> <?= $user->getFirstname(); ?> !</h1>
    <div class="row">
        <div class="col-md-6 col-12">
            <p class="card card-body mb-4 flex-row justify-content-between">
                Rang
                <span class="color-axianea"><?= UserType::$map[$user->getType()]['name']; ?></span>
            </p>
        </div>
    </div>
    <form class="" method="POST" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-9 col-12 mb-4">
                <section class="card card-fix">
                    <h4 class="card-header">Informations de compte</h4>
                    <section class="card-body">
                        <section class="row">
                            <div class="col-12 col-md">
                                <div class="form-group">
                                    <label class="bmd-label-floating" for="name">Nom</label>
                                    <input type="text" name="name" id="name" class="form-control" value="<?= $user->getName();?>" required="required">
                                </div>
                                <div class="form-group">
                                    <label class="bmd-label-floating" for="firstname">Prénom</label>
                                    <input type="text" name="firstname" id="firstname" class="form-control" value="<?= $user->getFirstname();?>" required="required">
                                </div>
                                <a class=" m-0 btn btn-link" href="<?= \Project\PageManager::getPageUrl('password_modifier'); ?>">Changer mon mot de passe</a>
                            </div>

                            <div class="col-12 col-md">
                                <div class="form-group">
                                    <label class="bmd-label-floating" for="mail">Adresse Mail</label>
                                    <input type="email" name="mail" id="mail" class="form-control" disabled value="<?= $user->getMail();?>" required="required">
                                </div>
                                <div class="form-group">
                                    <label class="bmd-label-floating" for="mail_secondary">Adresse Mail secondaire</label>
                                    <input type="email" name="mail_secondary" id="mail_secondary" class="form-control"  value="<?= $user->getMailSecondary();?>" required="required">
                                </div>
                            </div>
                        </section>
                    </section>
                </section>
            </div>
            <div class="col-lg-3 col-12 mb-4">
                <section class="card card-fix">
                    <h4 class="card-header">Image de Profil</h4>
                    <section class="card-body">
                        <section class="form-group col">
                            <label class="bmd-label-floating" for="logo">Choisir un logo</label>
                            <input type="file" class="form-control" name="new_logo" id="logo">
                            <p class="m-0 text-muted text-center">Image actuelle</p>
                            <img class="d-block m-auto" style="height: 6em;" src="<?= \Project\PageManager::getAsset('images', $user->getIcon(false));?>" alt="">
                            <input type="hidden" name="logo" value="<?= $user->getIcon(false); ?>">
                        </section>
                    </section>
                </section>
            </div>
        </div>

        <section class="mb-4 card">
            <h4 class="card-header">Informations professionnelles</h4>
            <section class="card-body">
                <section class="row">
                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <label class="bmd-label-floating" for="address">Adresse</label>
                            <input type="text" name="address" id="address" class="form-control" value="<?= $user->getAddress();?>"  required="required">
                        </div>
                    </div>
                    <div class="col-md-3 col-12">
                        <div class="form-group">
                        <label class="bmd-label-floating" for="zipcode">Code Postal</label>
                        <input type="text" name="zipcode" id="zipcode" class="form-control" value="<?= $user->getZipcode();?>" required="required">
                        </div>
                    </div>
                    <div class="col-md-3 col-12">
                        <div class="form-group">
                            <label class="bmd-label-floating" for="city">Ville</label>
                            <input type="text" name="city" id="city" class="form-control" value="<?= $user->getCity();?>" required="required">
                        </div>
                    </div>
                </section>

                <section class="row">
                    <div class="col-12 col-md">
                        <div class="form-group">
                        <label class="bmd-label-floating" for="phone_main">Téléphone</label>
                        <input type="tel" name="phone_main" id="phone_main" class="form-control" value="<?= DataConverter::phone($user->getPhoneMain());?>" required="required">
                        </div>
                    </div>
                    <div class="col-12 col-md">
                        <div class="form-group">
                        <label class="bmd-label-floating" for="phone_pro">Téléphone Pro</label>
                        <input type="tel" name="phone_pro" id="phone_pro" class="form-control" value="<?= DataConverter::phone($user->getPhonePro());?>" required="required">
                        </div>
                    </div>
                </section>
            </section>

            <button class="btn btn-axianea mb-4 ml-auto d-block">Mettre à jour</button>
        </section>
    </form>
</section>