<section class="container">
    <p class="alert alert-primary rounded text-white text-center" style="border-radius: 0.825rem!important;background: #31b9bf;">Oups ! Il semblerait que la princesse soit dans un autre château.
        Pour retrouver votre chemin, <a class="text-white" href="<?= \Project\PageManager::router('dashboard')?>">cliquez ici</a>
    </p>
</section>