<section class="container py-4">
    <section class="col-auto">
        <img src="https://www.themarysue.com/wp-content/uploads/2011/11/Our-Princess-is-in-Another-Castle.jpeg" class="p-0 col-6 img-responsive d-block m-auto" alt="">
        <p class="col-6 d-block mx-auto my-1 alert alert-primary rounded text-white text-center" style="border-radius: 0.825rem!important;background: #31b9bf;">Oups ! Il semblerait que la princesse soit dans un autre château.
            Pour retrouver votre chemin, <a class="text-white" href="<?= \Project\PageManager::getPageUrl('dashboard')?>">cliquez ici</a>
        </p>
    </section>
</section>