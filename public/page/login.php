<?php
use Project\Security\ConnexionManager;
use Project\Templates\AbstractTemplate;
?>
<section class="container-fluid row justify-content-center">
    <div class="col-sm-auto col-12">
        <section class="card my-4">
            <form class="card-body" method="POST">
                <h1>Espace de Connexion</h1>
                <section class="">
                    <div class="form-group">
                        <label class="bmd-label-floating" for="mail">Mail</label>
                        <input type="email" name="mail" id="mail" class="form-control"
                               value="<?= AbstractTemplate::renderCondition(ConnexionManager::getRequest('mail'), ConnexionManager::getRequest('mail'));?>"
                               required="required">
                    </div>
                    <div class="form-group">
                        <label class="bmd-label-floating" for="password">Mot de Passe</label>
                        <input type="password" name="password" id="password" class="form-control" required="required">
                    </div>
                    <section class="row mt-4">
                        <button class="btn btn-axianea ml-auto">Connexion</button>
                    </section>

                    <a class="btn d-block ml-auto" href="<?= \Project\PageManager::router('register'); ?>">Nouveau ? Inscription !</a>
                    <a class="btn d-block ml-auto" href="<?= \Project\PageManager::router('password_forget'); ?>">J'ai perdu mon mot de passe !</a>
                </section>
            </form>
        </section>
    </div>
</section>