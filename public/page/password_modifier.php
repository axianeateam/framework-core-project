<section class="container-fluid row justify-content-center">
    <section class="card my-4 col-auto">
        <form class="card-body" method="POST">
            <h4>Changement de mot de passe</h4>
            <input type="text" name="mail" hidden value="<?= $mail; ?>">
            <input type="text" name="token" hidden value="<?= $token; ?>">
            <label for="newPassword">Nouveau Mot de Passe</label>
            <input class="form-control" type="password" name="newPassword" id="newPassword" required>

            <button class="btn btn-axianea my-2 d-block ml-auto">Changer le mot de passe</button>
        </form>
    </section>
</section>