<?php
require __DIR__ . '/../vendor/autoload.php';

use Dotenv\Dotenv;
use Project\Security\ConnexionManager;
use Project\PageManager;
use Project\Templates\DeveloperDebugPanel;
use Project\Utilities\SimpleTimePerfTest;


Dotenv::create(__DIR__.'/../configs')->load();

$PerformanceMainPageProcess = new SimpleTimePerfTest();
PageManager::initializeConnexion();
session_start();

if(!ConnexionManager::getSecurity()->checkCurrentRouteSecurity()) { exit; }
ConnexionManager::checkAuth();
PageManager::updateLastRoute();

PageManager::getController();

?>
<!DOCTYPE HTML>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="<?= PageManager::getAsset('images','logo-cresus-compact.png')?>">
    <title><?= sprintf('%s - %s', PageManager::getSiteName(), PageManager::getRouter()->getCurrentRoute()->getNiceTitle());?></title>
    <?php PageManager::getStyles(); ?>
    <?php PageManager::getScripts(); ?>
</head>
<body class="container-fluid <?= PageManager::getPage();?>">
<?php PageManager::getHeader();  ?>
<main class="row">
    <aside class="col-2 p-0 nav-col">
        <?php PageManager::getNavigation(); ?>
    </aside>
    <section class="col-10" id="main-content">
        <section class="container-fluid">
            <section>
                <?php ConnexionManager::showFlashSessionMessages(); ?>
            </section>
            <?php PageManager::loadPage(); ?>
            <?php PageManager::getFooter(); ?>
        </section>
    </section>
</main>
<script type="application/javascript" src="<?= PageManager::getAsset('scripts', 'navigation.js');?>"></script>

<?php
$devDebug = ConnexionManager::getFromSession('dev_debug_mode');
if(ConnexionManager::getFromSession('dev_debug_mode')) {
    DeveloperDebugPanel::renderPanel($PerformanceMainPageProcess);
}
?>

</body>
</html>