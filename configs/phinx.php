<?php

// load our environment files - used to store credentials & configuration

use Dotenv\Dotenv;

Dotenv::create(__DIR__)->load();

return [
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/../db/migrations/',
    ],
    'environments' =>  [
        'default_database' => $_ENV['DATABASE_NAME'],
        'default_migration_table' => 'phinxlog',
        'development' => [
            'adapter' => 'mysql',
            'host' => $_ENV['DATABASE_HOST'],
            'name' => $_ENV['DATABASE_NAME'],
            'user' => $_ENV['DATABASE_USER'],
            'pass' => $_ENV['DATABASE_PASSWORD'],
            'port' => $_ENV['DATABASE_PORT'],
            'unix_socket' => $_ENV['DATABASE_UNIX_SOCKET'],
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ]
    ]
];